package ru.ars.web.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:02 20.05.2017
 */
@Entity
@Table(name = "player")
public class Player implements Serializable{
    private Long id;
    private String name;
    private String surname;
    private String city;
    private String biography;
    private Date birthday;
    private Integer height;
    private Integer weight;
    private Integer number;

    private Position position;
    private Team team;
    private Trainer trainer;
    private PhotoPlayer photoPlayer;

    public Player() {
    }

    public Player(String name, String surname, String city, String biography, Date birthday, Integer height, Integer weight, Integer number, Position position, Team team, Trainer trainer, PhotoPlayer photoPlayer) {
        this.name = name;
        this.surname = surname;
        this.city = city;
        this.biography = biography;
        this.birthday = birthday;
        this.height = height;
        this.weight = weight;
        this.number = number;
        this.position = position;
        this.team = team;
        this.trainer = trainer;
        this.photoPlayer = photoPlayer;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "surname", nullable = false, length = 20)
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "city", nullable = false, length = 30)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "biography", nullable = false, length = 5000)
    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    @Column(name = "birthday", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Column(name = "height", nullable = false)
    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Column(name = "weight", nullable = false)
    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Column(name = "number", nullable = false)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_position", referencedColumnName = "id")
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_team", referencedColumnName = "id")
    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_trainer", referencedColumnName = "id")
    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_photo", referencedColumnName = "id")
    public PhotoPlayer getPhotoPlayer() {
        return photoPlayer;
    }

    public void setPhotoPlayer(PhotoPlayer photoPlayer) {
        this.photoPlayer = photoPlayer;
    }

    @Override
    public String toString() {
        return "Player " + "id = " + id + ", name = " + name +
                ", surname = " + surname + ", city = " + city +
                ", biography = " + biography + ", birthday = " + birthday +
                ", height = " + height + ", weight = " + weight +
                ", number = " + number + ", position = " + position +
                ", team = " + team + ", trainer = " + trainer;
    }
}

