package ru.ars.web.entities;


import javax.persistence.*;
import java.io.Serializable;

//import org.hibernate.annotations.NamedQueries;
//import org.hibernate.annotations.NamedQuery;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 07:05 20.05.2017
 */
@Entity
@Table(name = "photo_player")
@NamedQueries({
        @NamedQuery(name = "findPhoto", query = "select p.photo from PhotoPlayer p where p.id = :pid")
})
public class PhotoPlayer implements Serializable{
    private Long id;
    private byte[] photo;

    public PhotoPlayer() {
    }

    public PhotoPlayer(byte[] photo) {
        this.photo = photo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Lob
    @Column(name = "photo")
    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}
