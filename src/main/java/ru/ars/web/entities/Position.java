package ru.ars.web.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:05 20.05.2017
 */
@Entity
@Table(name = "position")
public class Position implements Serializable{
    private Long id;
    private String title;

    public Position() {
    }

    public Position(String title) {
        this.title = title;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "title", nullable = false, length = 13)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
