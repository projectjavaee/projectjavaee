package ru.ars.web.entities.authorization;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:06 20.05.2017
 */
@Entity
@Table(name = "role")
@NamedQuery(name = "findAllRoles", query = "select r from Role r")
public class Role {
    private Long id;
    private String role;
    private List<User> users;

    public Role() {
    }

    public Role(String role) {
        this.role = role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @NotNull
    @Column(name = "role", nullable = false, length = 10)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_role")
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
