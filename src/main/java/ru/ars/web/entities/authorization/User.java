package ru.ars.web.entities.authorization;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:05 20.05.2017
 */
@Entity
@Table(name = "user")
@NamedQueries({
        @NamedQuery(name = "findUser", query = "select u from User u where u.username = :fname"),
        @NamedQuery(name = "findAllUsers", query = "select u from User u"),
})
public class User {
    private Long id;
    private String username;
    private String password;
    private String passwordAgain;
    private Date dateRegistration;
    private Long id_role;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, Date dateRegistration, Long id_role) {
        this.username = username;
        this.password = password;
        this.dateRegistration = dateRegistration;
        this.id_role = id_role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @Column(name = "username", nullable = false, length = 20)
//    @Size(min = 5, max = 20)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @NotNull
    @Column(name = "password", nullable = false, length = 64)
//    @Size(min = 8, max = 64)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }

    @Column(name = "id_role")
    public Long getId_role() {
        return id_role;
    }

    public void setId_role(Long id_role) {
        this.id_role = id_role;
    }

    @Column(name = "date_registration")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Date dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    @Override
    public String toString() {
        return "User: id = " + id +
                ", username = " + username +
                ", password = " + password +
                ", passwordAgain = " + passwordAgain;
    }


}
