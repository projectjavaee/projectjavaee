package ru.ars.web.ejb;

import ru.ars.web.entities.PhotoPlayer;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 03:11 28.05.2017
 */
@Named
public class PhotoEJB implements Serializable{

    @Inject
    private EntityManager em;
    private List<PhotoPlayer> photoPlayers;

    public List<PhotoPlayer> getPhotoList() {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<PhotoPlayer> criteria = builder.createQuery(PhotoPlayer.class);
        Root<PhotoPlayer> p = criteria.from(PhotoPlayer.class);
        criteria.select(p);
        return em.createQuery(criteria).getResultList();
    }

    public byte[] getPhotoPlayer(Long id) {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<PhotoPlayer> criteria = builder.createQuery(PhotoPlayer.class);
        Root<PhotoPlayer> p = criteria.from(PhotoPlayer.class);
        criteria.select(p).where(builder.equal(p.get("id"), id));
        photoPlayers = em.createQuery(criteria).getResultList();

        if (photoPlayers.isEmpty()){
            return null;
        }
        return photoPlayers.get(0).getPhoto();

//        or

//        try {
//            return em.createQuery(criteria).getSingleResult().getPhoto();
//        }catch (NoResultException e){
//            return null;
//        }
    }
}
