package ru.ars.web.ejb;

import ru.ars.web.entities.Player;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 14:01 31.05.2017
 */
@Named
@SessionScoped
public class PlayerEJB implements Serializable{

    @Inject
    private EntityManager em;

    public List<Player> getPlayer(){

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Player> criteria = builder.createQuery(Player.class);
        Root<Player> playerRoot = criteria.from(Player.class);
        criteria.select(playerRoot);
        return em.createQuery(criteria).getResultList();
    }
}
