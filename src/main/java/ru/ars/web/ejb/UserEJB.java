package ru.ars.web.ejb;

import org.apache.commons.codec.digest.DigestUtils;
import ru.ars.web.entities.authorization.Role;
import ru.ars.web.entities.authorization.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 23:22 04.05.2017
 */
@Named
@Stateless
public class UserEJB implements Serializable{

    @Inject
    private EntityManager em;

    public User createUserSHA256(User user){

        user = new User(user.getUsername(), DigestUtils.sha256Hex(user.getPassword()), new Date(),1L);
        em.persist(user);
        return user;
    }

    public User createUser(User user){
        em.persist(user);
        return user;
    }

    public Role createRole(Role role){
        em.persist(role);
        return role;
    }

    public String findUser(User user){

        Query query = em.createNamedQuery("findUser");
        query.setParameter("fname", user.getUsername());
        return ((User)query.getSingleResult()).getUsername();
    }

    public List<User> findAllUsers(){
        return em.createNamedQuery("findAllUsers", User.class).getResultList();
    }

    public List<Role> findAllRoles(){
        return em.createNamedQuery("findAllRoles", Role.class).getResultList();
    }

}
