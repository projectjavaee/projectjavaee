package ru.ars.web.database;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 23:25 04.05.2017
 */
public class DBProducer {

    @Produces
    @PersistenceContext(unitName = "projectDB")
    private EntityManager em;
}
