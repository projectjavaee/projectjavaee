package ru.ars.web.controllers;

import ru.ars.web.entities.Player;
import ru.ars.web.ejb.PlayerEJB;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 04:38 06.05.2017
 */
@Named
@ViewScoped
public class PlayerController implements Serializable{

    @Inject
    private PlayerEJB playerEJB;
    private List<Player> players;
    private Player player;

    public List<Player> getPlayers(){
        if (players == null){
            players = playerEJB.getPlayer();
        }
        return players;
    }

    public Player getPlayer() {
        if (player == null){
            player = new Player();
        }
        return player;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
