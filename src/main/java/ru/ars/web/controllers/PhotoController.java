package ru.ars.web.controllers;

import org.omnifaces.cdi.GraphicImageBean;
import ru.ars.web.ejb.PhotoEJB;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 14:16 28.05.2017
 */
@GraphicImageBean
public class PhotoController {

    @Inject
    private PhotoEJB photoEJB;

    public byte[] get(Long id){
        return photoEJB.getPhotoPlayer(id);

    }
}
