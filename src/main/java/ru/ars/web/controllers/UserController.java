package ru.ars.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ars.web.ejb.UserEJB;
import ru.ars.web.entities.authorization.User;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

import static java.lang.String.format;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 23:22 04.05.2017
 */
@Named
@SessionScoped
public class UserController implements Serializable{
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final String WELCOME = "/pages/welcome.xhtml?faces-redirect=true";
    private static final String EXIT = "/index.xhtml?faces-redirect=true";
    private static final String RESULT = "/index.xhtml";

    @Inject
    private UserEJB userEJB;
    private User user;

//    ======================================
//    =========== Registration =============
//    ======================================

    public String doCreateUser(){

        userEJB.createUserSHA256(user);

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Регистрация прошла успешно",
                user.getUsername() + ", пожалуйста авторизуйтесь"));
        // TODO: 08.05.2017  Rewrite FacesContext.getCurrentInstance().addMessage in doCreateUser()

        LOGGER.info(format("Регистрация: %s прошла успешно, UserController.doCreateUser working", user));
        return RESULT;
    }

//    ======================================
//    =========== Authorization ============
//    ======================================

    public String login(){

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        try {

            if (request.getUserPrincipal() == null || (request.getUserPrincipal() != null && !request.getUserPrincipal().getName().equals(user.getUsername()))) {
                request.logout();
                request.login(user.getUsername(), user.getPassword());
            }
            LOGGER.info(format("Авторизация: %s прошла успешно, UserController.login() working ", user));
            return "welcome";

        }catch (ServletException e){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Неправильно введены имя пользователя или пароль", null));
            LOGGER.error(format("Ошибка авторизации - %s, UserController.login(), ServletException: %s ", user, e));
            // TODO: 08.05.2017 Rewrite FacesContext.getCurrentInstance().addMessage in login()
        }
        return RESULT;
    }

//    ======================================
//    ================ Exit ================
//    ======================================

    public String logout(){

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

        try {

            request.logout();
            context.getExternalContext().invalidateSession();


        } catch (ServletException e) {
            LOGGER.error(format("Ошибка выхода из сессии, UserController.logout(), ServletException: %s ", e));
//            FacesContext.getCurrentInstance().addMessage();
            // TODO: 08.05.2017 Add message in FacesContext.getCurrentInstance().addMessage();
        }

        return EXIT;
    }


    public User getUser() {
        if (user == null){
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
