package ru.ars.web.controllers;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Locale;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 23:28 01.06.2017
 */
@Named
@SessionScoped
public class LocaleSwitch implements Serializable{

    private Locale locale;

    public LocaleSwitch() {
    }

    public void localeSwitch(String localeCode) {
        locale = new Locale(localeCode);
    }

    public Locale getLocale() {
        if (locale == null){
            locale = new Locale("ru");
        }
        return locale;
    }
}
