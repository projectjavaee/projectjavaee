package ru.ars.web.placeholders;

import ru.ars.web.entities.PhotoPlayer;
import ru.ars.web.ejb.PhotoEJB;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 14:10 28.05.2017
 */
@Named
@RequestScoped
public class PhotoPlayerFilledDB {

    private List<PhotoPlayer> photo;

    @Inject
    private PhotoEJB photoEJB;

    @PostConstruct
    public void init(){
        photo = photoEJB.getPhotoList();
    }

    public List<PhotoPlayer> getPhoto() {
        return photo;
    }
}
