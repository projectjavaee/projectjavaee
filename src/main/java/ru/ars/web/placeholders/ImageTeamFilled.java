package ru.ars.web.placeholders;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 22:13 07.05.2017
 *
 * Временно отключен
 */
@Named
public class ImageTeamFilled {
    private List<String> images;

    @PostConstruct
    public void init(){
        images = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            images.add("team" + i + ".jpg");
        }
    }

    public List<String> getImages() {
        return images;
    }
}
