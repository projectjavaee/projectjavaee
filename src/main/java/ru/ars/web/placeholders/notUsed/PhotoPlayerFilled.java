package ru.ars.web.placeholders.notUsed;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 20:44 29.05.2017
 */
@Named
public class PhotoPlayerFilled {
    private List<String> images;

    @PostConstruct
    public void init(){
        images = new ArrayList<>();
        for (int i = 1; i <= 30; i++) {
            images.add("players" + i + ".jpg");
        }
    }

    public List<String> getImages() {
        return images;
    }
}
