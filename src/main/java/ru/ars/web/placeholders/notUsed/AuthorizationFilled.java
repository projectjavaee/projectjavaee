package ru.ars.web.placeholders.notUsed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ars.web.ejb.UserEJB;
import ru.ars.web.entities.authorization.Role;
import ru.ars.web.entities.authorization.User;

import java.util.Date;

/**
 * Developer: Ruslan Sabirov
 * Email: sabirov_ruslan@outlook.com
 * Date: 23:28 04.05.2017
 *
 * Нужно разкоментировать все закоментированные элементы!!!
 *
 * Класс автоматически разворачивает базу данных на сервере и создает двух пользователей для авторизации:
 * 1: login - admin; password - 12345678
 * 2: login - client; password - 12345678
 * либо можно воспользоваться формой регистрации на главной странице
 *
 */

//@Singleton
//@Startup
//@DataSourceDefinition(name = "jdbc/project", className = "com.mysql.jdbc.Driver",
//        url = "jdbc:mysql://localhost:3306/project", user = "root", password = "root",
//        portNumber = 3306, databaseName = "project"
//)
public class AuthorizationFilled {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilled.class);

//    @Inject
    private UserEJB userEJB;

//    @PostConstruct
    private void createDataProject(){

        userEJB.createRole(new Role("admin"));
        userEJB.createRole(new Role("user"));

        userEJB.createUserSHA256(new User("admin", "12345678", new Date(), 1L));
        userEJB.createUserSHA256(new User("client", "12345678", new Date(), 2L));

        LOGGER.info("AuthorizationFilled.createDataProject() inserted " + userEJB.findAllUsers().size()
                + " Users and " + userEJB.findAllRoles().size() + " Roles");
    }
}
